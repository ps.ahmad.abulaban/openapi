##### References
* https://dev.to/alfonzjanfrithz/spring-boot-rest-with-openapi-3-59jm
* https://editor.swagger.io/ (Validate openapi yaml file)
* https://openapi-generator.tech/docs/generators/spring/
* https://www.mokkapps.de/blog/how-to-generate-angular-and-spring-code-from-open-api-specification/
* https://www.baeldung.com/mapstruct , https://mapstruct.org/documentation/stable/reference/html
* https://swagger.io/docs/specification/describing-request-body/multipart-requests/
* https://reflectoring.io/spring-boot-openapi/
* https://medium.com/@mustafasaeed007/angular-localization-to-support-right-to-left-languages-7225a6c71eef