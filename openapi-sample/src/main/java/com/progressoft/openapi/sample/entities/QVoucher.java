package com.progressoft.openapi.sample.entities;

import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import java.math.BigDecimal;
import java.time.Instant;

public class QVoucher extends EntityPathBase<Voucher> {

    public static final QVoucher query = new QVoucher();

    public final StringPath id = createString("id");
    public final DateTimePath<Instant> timestamp = createDateTime("timestamp", Instant.class);
    public final StringPath party = createString("party");
    public final StringPath counterParty = createString("counterParty");
    public final NumberPath<BigDecimal> amount = createNumber("amount", BigDecimal.class);
    public final StringPath type = createString("type");
    public final QMessage message = new QMessage(forProperty("message"));

    private QVoucher() {
        super(Voucher.class, "query");
    }
}