package com.progressoft.openapi.sample.repositories;

import com.progressoft.openapi.sample.entities.Message;
import com.progressoft.openapi.sample.entities.Voucher;
import com.progressoft.openapi.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoucherRepository extends CrudRepository<Voucher, String>, ExtendedQuerydslPredicateExecutor<Voucher> {

    List<Voucher> findAllByMessage(Message message);
}
