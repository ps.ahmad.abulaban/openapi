package com.progressoft.openapi.sample.mapper.query;

import com.progressoft.openapi.sample.entities.QParticipant;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.StringPath;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Objects;

import static java.util.Optional.ofNullable;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParticipantQueryMapper {
    private static final QParticipant QUERY = QParticipant.query;

    public static Predicate toPredicate(String code, String bic, String name, String participantType) {
        BooleanBuilder where = new BooleanBuilder();

        ofNullable(code).ifPresent(p -> where.and(createPredicate(p, QUERY.code)));
        ofNullable(bic).ifPresent(p -> where.and(createPredicate(p, QUERY.bic)));
        ofNullable(name).ifPresent(p -> where.and(createPredicate(p, QUERY.name)));
        ofNullable(participantType).ifPresent(p -> where.and(createPredicate(p, QUERY.participantType)));

        return where;
    }

    private static BooleanExpression createPredicate(String p, StringPath path) {
        return Objects.nonNull(p) && p.length() > 0 && p.endsWith("*") ?
                path.containsIgnoreCase(p.substring(0, p.length() - 1)) : path.equalsIgnoreCase(p);
    }
}
