package com.progressoft.openapi.sample.aspects;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.AllArgsConstructor;
import lombok.extern.flogger.Flogger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Flogger
@Component
@AllArgsConstructor
public class ControllerMetricsAspect {
    private final MeterRegistry registry;
    private static final ThreadLocal<Long> START_TIME = new ThreadLocal<>();

    @Pointcut("within(com.progressoft.openapi.sample.api..*)")
    public void controllerPackage() {
    }

//    @Pointcut("@within(org.springframework.stereotype.Controller)")
//    public void controllerPointcut() {
//    }
//
//    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *)")
//    public void restControllerPointcut() {
//    }

    @Before("controllerPackage()")
    public void doCountBefore(JoinPoint joinPoint) {
        log.atInfo().log("Initiating metrics scrapping for %s ", getMetricsKey(joinPoint));
        Counter counter = registerCounter(getMetricsKey(joinPoint) + "_Hit_Count", "METHOD_HIT_COUNT", "SUCCESS");
        counter.increment();
        START_TIME.set(System.currentTimeMillis());
    }

    @After("controllerPackage()")
    public void doCalculateTimeElapsedAfter(JoinPoint joinPoint) {
        Timer timer = registerTimer(getMetricsKey(joinPoint) + "_Execution_Time", "TIME_ELAPSED", "SUCCESS");
        timer.record(() -> System.currentTimeMillis() - START_TIME.get());
        START_TIME.remove();
        log.atInfo().log("Metrics scrapping completed for %s ", getMetricsKey(joinPoint));
    }

    private Counter registerCounter(String name, String... tag) {
        return registry.counter(name, tag);
    }

    private Timer registerTimer(String name, String... tag) {
        return registry.timer(name, tag);
    }

    private String getMetricsKey(JoinPoint joinPoint) {
        return joinPoint.getSignature().getDeclaringType().getSimpleName() +
                "_" + joinPoint.getSignature().getName();
    }
}
