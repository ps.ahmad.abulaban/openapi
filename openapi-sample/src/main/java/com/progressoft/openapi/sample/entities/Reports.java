package com.progressoft.openapi.sample.entities;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.type.descriptor.sql.LobTypeMappings;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class Reports {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private String id;

    @Column(nullable = false)
    private LocalDate date;

    @Column(nullable = false)
    private String participantCode;

    @Lob
    @Column(nullable = false)
    private byte[] csvReport;

    @Lob
    @Column
    private byte[] xlsxReport;
}
