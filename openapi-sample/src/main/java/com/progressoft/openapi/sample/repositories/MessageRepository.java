package com.progressoft.openapi.sample.repositories;

import com.progressoft.openapi.sample.entities.Message;
import com.progressoft.openapi.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends CrudRepository<Message, String>, ExtendedQuerydslPredicateExecutor<Message> {
}
