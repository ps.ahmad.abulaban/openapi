package com.progressoft.openapi.sample.mapper.request;

import com.progressoft.openapi.sample.entities.Participant;
import com.progressoft.openapi.sample.model.ParticipantCreationRequest;
import com.progressoft.openapi.sample.model.ParticipantModificationRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.openapitools.jackson.nullable.JsonNullable;

import java.util.Base64;
import java.util.Objects;

@Mapper(componentModel = "spring")
public interface ParticipantRequestMapper {

    Participant toParticipant(ParticipantCreationRequest request);

    @Mappings({
            @Mapping(target = "code", source = "originalParticipant.code"),
            @Mapping(target = "bic", source = "originalParticipant.bic"),
            @Mapping(target = "shortName", source = "originalParticipant.shortName"),
            @Mapping(target = "name", source = "request.name"),
            @Mapping(target = "participantType", source = "originalParticipant.participantType"),
            @Mapping(target = "settlementBank", source = "request.settlementBank"),
            @Mapping(target = "logo", source = "request.logo")})
    Participant toParticipant(ParticipantModificationRequest request, Participant originalParticipant);

    default byte[] map(String value) {
        if (!Objects.isNull(value))
            return Base64.getDecoder().decode(value);
        return null;
    }

    default String map(JsonNullable<String> value) {
        if (value.isPresent())
            return value.get();
        return null;
    }
}
