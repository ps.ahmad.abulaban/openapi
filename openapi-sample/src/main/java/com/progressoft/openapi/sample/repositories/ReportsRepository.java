package com.progressoft.openapi.sample.repositories;

import com.progressoft.openapi.sample.entities.Reports;
import com.progressoft.openapi.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReportsRepository extends CrudRepository<Reports, String>, ExtendedQuerydslPredicateExecutor<Reports> {

}
