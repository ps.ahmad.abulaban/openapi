package com.progressoft.openapi.sample.mapper.request;

import com.progressoft.openapi.sample.entities.Reports;
import com.progressoft.openapi.sample.model.ReportsDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ReportsMapper {

    @Mappings({
            @Mapping(target = "date", source = "details.date"),
            @Mapping(target = "participantCode", source = "details.participantCode"),
            @Mapping(target = "csvReport", source = "csvReport"),
            @Mapping(target = "xlsxReport", source = "xlsxReport")})
    Reports toReports(ReportsDetails details, byte[] csvReport, byte[] xlsxReport);
}
