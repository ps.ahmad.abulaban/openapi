package com.progressoft.openapi.sample.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.ByteArrayInputStream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileDownloadResponseBuilder {

    public static ResponseEntity<Resource> createDownloadResponse(byte[] fileContent, String fileName, String fileType) {
        return ResponseEntity.ok()
                .header("content-disposition", "attachment; filename=" + fileName + "." + fileType)
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .contentLength(fileContent.length)
                .body(new InputStreamResource(new ByteArrayInputStream(fileContent)));
    }
}
