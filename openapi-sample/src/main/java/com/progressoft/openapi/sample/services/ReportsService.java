package com.progressoft.openapi.sample.services;

import com.progressoft.openapi.sample.api.ReportsApiDelegate;
import com.progressoft.openapi.sample.entities.QReports;
import com.progressoft.openapi.sample.entities.Reports;
import com.progressoft.openapi.sample.mapper.request.ReportsMapper;
import com.progressoft.openapi.sample.mapper.response.ReportsResponseMapper;
import com.progressoft.openapi.sample.model.PageRequest;
import com.progressoft.openapi.sample.model.ReportsDetails;
import com.progressoft.openapi.sample.model.ReportsPage;
import com.progressoft.openapi.sample.model.ReportsResponse;
import com.progressoft.openapi.sample.repositories.ReportsRepository;
import com.progressoft.openapi.sample.utils.PageMapper;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.QBean;
import lombok.AllArgsConstructor;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.hateoas.Link;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.progressoft.openapi.sample.utils.FileDownloadResponseBuilder.createDownloadResponse;

@Service
@AllArgsConstructor
public class ReportsService implements ReportsApiDelegate {

    private final ReportsRepository repository;
    private final ReportsMapper requestMapper;
    private final ReportsResponseMapper responseMapper;
    private final NativeWebRequest nativeWebRequest;
    private final Environment env;

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.ofNullable(nativeWebRequest);
    }

    @Override
    public ResponseEntity<ReportsPage> getReports(PageRequest pageRequest) {
        System.out.println(pageRequest.toString());
        QBean<Reports> reportsQBean = QReports.getQBean();
        Page<Reports> resultedPage = repository.findAll(reportsQBean
                , new BooleanBuilder()
                , PageMapper.createPageRequest(pageRequest));
        List<ReportsResponse> reportsResponses = resultedPage.stream()
                .map(responseMapper::toReportsResponse)
                .collect(Collectors.toList());
        ReportsPage reportsPage = new ReportsPage();
        reportsPage.setContent(reportsResponses);
        reportsPage.setPage(PageMapper.createPageMetaData(resultedPage));
        return ResponseEntity.ok(reportsPage);
    }

    @Override
    public ResponseEntity<ReportsResponse> getReport(String reportId) {
        return repository.findById(reportId)
                .map(this::mapToReportsResponseAndAddLinks)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<String> uploadReports(ReportsDetails details, MultipartFile csvReport, MultipartFile xlsxReport) {
        try {
            Reports reports = requestMapper.toReports(details, csvReport.getBytes(), xlsxReport.isEmpty() ? null : xlsxReport.getBytes());
            return ResponseEntity.ok(repository.save(reports).getId());
        } catch (IOException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @Override
    public ResponseEntity<Resource> downloadReport(String reportId, String reportType) {
        Optional<Reports> reports = repository.findById(reportId);
        if (reports.isPresent()) {
            if (reportType.equals("csv"))
                return createDownloadResponse(reports.get().getCsvReport(), reportId, "csv");
            if (reportType.equals("xlsx"))
                return createDownloadResponse(reports.get().getXlsxReport(), reportId, "xlsx");
        }
        return ResponseEntity.notFound().build();
    }

    private ReportsResponse mapToReportsResponseAndAddLinks(Reports r) {
        String baseUrl = getBaseUrl();
        ReportsResponse reportsResponse = responseMapper.toReportsResponse(r);
        if (!Objects.isNull(r.getCsvReport()))
            reportsResponse.add(Link.of(getLinkHref(r.getId(), baseUrl, "csv"), getLinkRel(r.getId(), "csv")).withTitle(getLinkTitle(r.getId(), "csv")));
        if (!Objects.isNull(r.getXlsxReport()))
            reportsResponse.add(Link.of(getLinkHref(r.getId(), baseUrl, "xlsx"), getLinkRel(r.getId(), "xlsx")).withTitle(getLinkTitle(r.getId(), "xlsx")));
        return reportsResponse;
    }

    private String getBaseUrl() {
        return "http://" + getRequest().get().getHeader("host") + env.getProperty("server.servlet.context-path") + env.getProperty("openapi.openAPISample.base-path") + "/reports";
    }

    private String getLinkTitle(String id, String reportType) {
        return id + "-" + reportType;
    }

    private String getLinkRel(String id, String reportType) {
        return "download-" + id + "-" + reportType;
    }

    private String getLinkHref(String id, String baseUrl, String reportType) {
        return baseUrl + "/" + id + "/" + reportType;
    }
}
