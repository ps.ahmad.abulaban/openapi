package com.progressoft.openapi.sample.entities;

import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.QBean;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.StringPath;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class QReports extends EntityPathBase<Reports> {

    public static final QReports query = new QReports();

    public final StringPath id = createString("id");
    public final DateTimePath<LocalDate> date = createDateTime("date", LocalDate.class);
    public final StringPath participantCode = createString("participantCode");

    private QReports() {
        super(Reports.class, "query");
    }

    public static QBean<Reports> getQBean() {
        return Projections.bean(Reports.class, QReports.getSelectedFields());
    }

    private static Expression<?>[] getSelectedFields() {
        List<Expression<?>> expressionList = new ArrayList<>();
        expressionList.add(query.id);
        expressionList.add(query.date);
        expressionList.add(query.participantCode);
        return expressionList.toArray(new Expression<?>[0]);
    }
}