package com.progressoft.openapi.sample.mapper.response;

import com.progressoft.openapi.sample.entities.Reports;
import com.progressoft.openapi.sample.model.ReportsResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReportsResponseMapper {

    ReportsResponse toReportsResponse(Reports reports);
}
