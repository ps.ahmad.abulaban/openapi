package com.progressoft.openapi.sample.entities;

import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import java.math.BigDecimal;

public class QPosition extends EntityPathBase<Position> {

    public static final QPosition query = new QPosition();

    public final StringPath id = createString("id");
    public final StringPath participantCode = createString("participantCode");
    public final NumberPath<BigDecimal> netPosition = createNumber("netPosition", BigDecimal.class);
    public final StringPath creditDebitIndicator = createString("creditDebitIndicator");

    private QPosition() {
        super(Position.class, "query");
    }
}