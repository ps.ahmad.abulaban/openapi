package com.progressoft.openapi.sample.mapper.response;

import com.progressoft.openapi.sample.entities.Participant;
import com.progressoft.openapi.sample.model.ParticipantResponse;
import org.mapstruct.Mapper;

import java.util.Base64;
import java.util.Objects;

@Mapper(componentModel = "spring")
public interface ParticipantResponseMapper {

    ParticipantResponse toParticipantResponse(Participant participant);

    default String map(byte[] value) {
        if (!Objects.isNull(value))
            return new String(Base64.getEncoder().encode(value));
        return null;
    }
}
