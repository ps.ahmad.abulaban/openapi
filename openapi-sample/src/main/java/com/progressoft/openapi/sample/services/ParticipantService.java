package com.progressoft.openapi.sample.services;

import com.progressoft.openapi.sample.api.ParticipantsApiDelegate;
import com.progressoft.openapi.sample.entities.Participant;
import com.progressoft.openapi.sample.entities.QParticipant;
import com.progressoft.openapi.sample.mapper.query.ParticipantQueryMapper;
import com.progressoft.openapi.sample.mapper.request.ParticipantRequestMapper;
import com.progressoft.openapi.sample.mapper.response.ParticipantResponseMapper;
import com.progressoft.openapi.sample.model.*;
import com.progressoft.openapi.sample.repositories.ParticipantRepository;
import com.progressoft.openapi.sample.utils.PageMapper;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.QBean;
import lombok.AllArgsConstructor;
import org.openapitools.jackson.nullable.JsonNullable;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidationException;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ParticipantService implements ParticipantsApiDelegate {

    private static final Pattern SELECTED_FIELDS_PATTERN = Pattern.compile("(code|bic|name|shortName|participantType|settlementBank|logo){1}((,){1}(code|bic|name|shortName|participantType|settlementBank|logo){1})*");

    private final ParticipantRepository repository;
    private final ParticipantResponseMapper responseMapper;
    private final ParticipantRequestMapper requestMapper;

    @Override
    public ResponseEntity<ParticipantResponse> getParticipant(String participantCode) {
        return repository.findById(participantCode)
                .map(responseMapper::toParticipantResponse)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<ParticipantsPage> getParticipants(String selectedFields, PageRequest pageRequest, String code, String bic, String name, String participantType) {
        Predicate predicate = ParticipantQueryMapper.toPredicate(code, bic, name, participantType);
        Page<Participant> resultedPage;
        if (isSelectedFieldsHeaderFilledAndValid(selectedFields)) {
            QBean<Participant> participantQBean = QParticipant.getQBean(Arrays.asList(selectedFields.split(",")));
            resultedPage = repository.findAll(participantQBean
                    , predicate
                    , PageMapper.createPageRequest(pageRequest));
        } else
            resultedPage = repository.findAll(predicate
                    , PageMapper.createPageRequest(pageRequest));
        List<ParticipantResponse> participantResponses = resultedPage.stream()
                .map(responseMapper::toParticipantResponse)
                .collect(Collectors.toList());
        ParticipantsPage participantPage = new ParticipantsPage();
        participantPage.setContent(participantResponses);
        participantPage.setPage(PageMapper.createPageMetaData(resultedPage));
        return ResponseEntity.ok(participantPage);
    }

    @Override
    public ResponseEntity<ParticipantResponse> createParticipant(ParticipantCreationRequest participantCreationRequest) {
        validateCreationRequest(participantCreationRequest);
        Participant participant = repository.save(requestMapper.toParticipant(participantCreationRequest));
        return ResponseEntity.ok(responseMapper.toParticipantResponse(participant));
    }

    @Override
    public ResponseEntity<ParticipantResponse> modifyParticipant(String participantCode, ParticipantModificationRequest participantModificationRequest) {
        Optional<Participant> participant = repository.findById(participantCode);
        validateModificationRequest(participant, participantModificationRequest);
        Participant modifiedParticipant = repository.save(requestMapper.toParticipant(participantModificationRequest, participant.get()));
        return ResponseEntity.ok(responseMapper.toParticipantResponse(modifiedParticipant));
    }

    private void validateModificationRequest(Optional<Participant> participant, ParticipantModificationRequest request) {
        javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ParticipantModificationRequest>> violations = validator.validate(request);
        if (!violations.isEmpty())
            throw new ValidationException(violations.iterator().next().getMessage());
        if (participant.isEmpty())
            throw new ValidationException("Participant not exist");
        validateSettlementBank(request.getSettlementBank(), participant.get().getParticipantType());
    }

    private void validateCreationRequest(ParticipantCreationRequest request) {
        javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ParticipantCreationRequest>> violations = validator.validate(request);
        if (!violations.isEmpty())
            throw new ValidationException(violations.iterator().next().getMessage());
        if (repository.existsById(request.getCode()))
            throw new ValidationException("Participant already exist");
        if (request.getParticipantType().equals("Direct"))
            validateDirectParticipant(request);
        else if (request.getParticipantType().equals("Indirect"))
            validateIndirectParticipant(request);
        else
            validateOperatorParticipant(request);
    }

    private void validateOperatorParticipant(ParticipantCreationRequest request) {
        if (!request.getBic().isPresent())
            throw new ValidationException("Bic required for indirect participant");
        validateSettlementBank(request.getSettlementBank(), request.getParticipantType());
    }

    private void validateIndirectParticipant(ParticipantCreationRequest request) {
        if (request.getBic().isPresent())
            throw new ValidationException("Bic should be null when participant type is indirect");
        validateSettlementBank(request.getSettlementBank(), request.getParticipantType());
    }

    private void validateSettlementBank(JsonNullable<String> settlementBank, String participantType) {
        if (!settlementBank.isPresent())
            throw new ValidationException("Settlement bank required for " + participantType + "participant");
        if (!repository.existsByCodeAndParticipantType(settlementBank.get(), "Direct"))
            throw new ValidationException("Settlement bank not exist as direct participant");
    }

    private void validateDirectParticipant(ParticipantCreationRequest request) {
        if (!request.getBic().isPresent())
            throw new ValidationException("Bic required for direct participant");
        if (request.getSettlementBank().isPresent())
            throw new ValidationException("Settlement bank should be null when participant type is direct");
    }

    private boolean isSelectedFieldsHeaderFilledAndValid(String selectedFields) {
        if (Objects.isNull(selectedFields) || selectedFields.trim().isEmpty())
            return false;
        return SELECTED_FIELDS_PATTERN.matcher(selectedFields).matches();
    }
}
