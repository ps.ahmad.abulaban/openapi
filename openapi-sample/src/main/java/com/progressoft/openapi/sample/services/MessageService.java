package com.progressoft.openapi.sample.services;

import com.progressoft.openapi.sample.api.MessagesApiDelegate;
import com.progressoft.openapi.sample.entities.Message;
import com.progressoft.openapi.sample.mapper.response.MessageResponseMapper;
import com.progressoft.openapi.sample.model.MessageResponse;
import com.progressoft.openapi.sample.model.MessagesPage;
import com.progressoft.openapi.sample.model.PageRequest;
import com.progressoft.openapi.sample.repositories.MessageRepository;
import com.progressoft.openapi.sample.utils.PageMapper;
import com.querydsl.core.BooleanBuilder;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class MessageService implements MessagesApiDelegate {

    private final MessageRepository repository;
    private final MessageResponseMapper responseMapper;

    @Override
    public ResponseEntity<MessagesPage> getMessages(PageRequest pageRequest) {
        Page<Message> resultedPage = repository.findAll(new BooleanBuilder(),
                PageMapper.createPageRequest(pageRequest));
        List<MessageResponse> messageResponses = resultedPage.stream()
                .map(responseMapper::toMessageResponse)
                .collect(Collectors.toList());
        MessagesPage messagesPage = new MessagesPage();
        messagesPage.setContent(messageResponses);
        messagesPage.setPage(PageMapper.createPageMetaData(resultedPage));
        return ResponseEntity.ok(messagesPage);
    }

    @Override
    public ResponseEntity<MessageResponse> getMessage(String messageId) {
        return repository.findById(messageId)
                .map(responseMapper::toMessageResponse)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }
}
