package com.progressoft.openapi.sample.utils;

import com.progressoft.openapi.sample.model.Page;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PageMapper {
    public static <T> Page createPageMetaData(org.springframework.data.domain.Page<T> page) {
        Page resultPage = new Page();
        resultPage.setNumber(page.getNumber());
        resultPage.setSize(page.getSize());
        resultPage.setTotalElements(page.getTotalElements());
        resultPage.setTotalPages(page.getTotalPages());
        return resultPage;
    }

    public static PageRequest createPageRequest(com.progressoft.openapi.sample.model.PageRequest pageRequest) {
        Sort sort = Sort.unsorted();
        if (!Objects.isNull(pageRequest.getSortBy()))
            sort = Sort.by(Sort.Direction.fromString(pageRequest.getSortDirection()), pageRequest.getSortBy().toArray(new String[0]));
        return PageRequest.of(pageRequest.getPage(), pageRequest.getSize(), sort);
    }
}
