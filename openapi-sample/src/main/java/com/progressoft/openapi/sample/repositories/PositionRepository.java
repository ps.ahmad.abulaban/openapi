package com.progressoft.openapi.sample.repositories;

import com.progressoft.openapi.sample.entities.Position;
import com.progressoft.openapi.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PositionRepository extends CrudRepository<Position, String>, ExtendedQuerydslPredicateExecutor<Position> {
    Optional<Position> findByParticipantCode(String participantCode);
}
