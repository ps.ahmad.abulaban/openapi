package com.progressoft.openapi.sample.mapper.response;

import com.progressoft.openapi.sample.entities.Message;
import com.progressoft.openapi.sample.model.MessageResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {VoucherResponseMapper.class})
public interface MessageResponseMapper {

    MessageResponse toMessageResponse(Message message);
}
