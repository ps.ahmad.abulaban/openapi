package com.progressoft.openapi.sample.repositories;

import com.progressoft.openapi.sample.entities.Participant;
import com.progressoft.openapi.sample.repositories.jpa.ExtendedQuerydslPredicateExecutor;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipantRepository extends CrudRepository<Participant, String>, ExtendedQuerydslPredicateExecutor<Participant> {
    @Override
    List<Participant> findAll();

    boolean existsByCodeAndParticipantType(String code, String participantType);

    @Override
    Page<Participant> findAll(Expression<Participant> expression, Predicate predicate, Pageable pageable);

    @Override
    Page<Participant> findAll(Predicate predicate, Pageable pageable);
}
