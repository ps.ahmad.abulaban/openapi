package com.progressoft.openapi.sample.aspects;

import com.google.common.flogger.LazyArgs;
import lombok.extern.flogger.Flogger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Flogger
@Component
public class RepositoryAspect {

    @Pointcut("@within(org.springframework.stereotype.Repository)")
    public void repositoryClassPointCut() {
    }

    @Around("repositoryClassPointCut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
//        log.atInfo().log("Invoking Repository method : %s.%s() ",
//                joinPoint.getSignature().getDeclaringType().getSimpleName(),
//                joinPoint.getSignature().getName());
        log.atInfo().log("Invoking Repository method : %s ",
                joinPoint.getSignature().toString());
        log.atFine().log("with argument[%s] ", LazyArgs.lazy(() -> Arrays.toString(joinPoint.getArgs())));
        return joinPoint.proceed();
    }
}
