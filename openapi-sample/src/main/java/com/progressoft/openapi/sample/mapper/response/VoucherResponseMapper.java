package com.progressoft.openapi.sample.mapper.response;

import com.progressoft.openapi.sample.entities.Voucher;
import com.progressoft.openapi.sample.model.VoucherResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface VoucherResponseMapper {

    @Mappings({
            @Mapping(target = "timestamp", source = "voucher.timestamp",
                    dateFormat = "dd-MM-yyyy HH:mm:ss")})
    VoucherResponse toVoucherResponse(Voucher voucher);

}
