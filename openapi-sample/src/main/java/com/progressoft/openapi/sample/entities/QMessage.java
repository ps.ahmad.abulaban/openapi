package com.progressoft.openapi.sample.entities;

import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;

import java.math.BigDecimal;
import java.time.Instant;

public class QMessage extends EntityPathBase<Message> {

    public static final QMessage query = new QMessage();

    public final StringPath id = createString("id");
    public final DateTimePath<Instant> timestamp = createDateTime("timestamp", Instant.class);
    public final StringPath sourceBankCode = createString("sourceBankCode");
    public final StringPath destinationBankCode = createString("destinationBankCode");
    public final NumberPath<BigDecimal> transactionAmount = createNumber("transactionAmount", BigDecimal.class);
    public final NumberPath<BigDecimal> fees = createNumber("fees", BigDecimal.class);
    public final NumberPath<BigDecimal> vat = createNumber("vat", BigDecimal.class);

    private QMessage() {
        super(Message.class, "query");
    }

    QMessage(PathMetadata metadata) {
        super(Message.class, metadata);
    }
}