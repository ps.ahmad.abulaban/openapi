import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatExpansionPanel} from '@angular/material/expansion';

@Component({
  selector: 'app-filter-panel',
  templateUrl: 'filter-panel.component.html'
})
export class FilterPanelComponent implements OnInit {

  @Output() applyEmitter = new EventEmitter<any>();
  @Output() resetEmitter = new EventEmitter<any>();
  @Input() expanded = false;
  @ViewChild(MatExpansionPanel, {static: true}) panel: MatExpansionPanel;

  resetDefaults(): void {
    this.resetEmitter.emit();
  }

  applyFilter(): void {
    this.applyEmitter.emit();
  }

  ngOnInit(): void {
  }
}
