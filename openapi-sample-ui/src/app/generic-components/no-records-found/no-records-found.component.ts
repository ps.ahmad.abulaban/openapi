import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-no-records-found',
  templateUrl: './no-records-found.component.html',
  styleUrls: []
})
export class NoRecordsFoundComponent {

  @Input() dataLength: number;
  @Input() dataAvailable = true;

}
