import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ParticipantsGridComponent} from './participants/grid/participants-grid.component';
import {ParticipantsDetailsComponent} from './participants/details/participants-details.component';
import {ParticipantsCreationComponent} from "./participants/creation/participants-creation.component";

const routes: Routes = [
  {path: '', redirectTo: 'participants', pathMatch: 'full'},
  {
    path: 'participants',
    children: [
      {path: '', component: ParticipantsGridComponent, data: {title: 'participantsGrid'}},
      {path: 'create', component: ParticipantsCreationComponent, data: {title: 'participantsCreation'}},
      {
        path: ':code',
        children: [
          {
            path: '', component: ParticipantsDetailsComponent, data: {title: 'participantsDetails'},
          }
        ]
      }
      // {path: 'create', component: ParticipantCreationRequestComponent, data: {title: 'Participant Creation Request'}},
      // {
      //   path: ':code',
      //   children: [
      //     {
      //       path: '', component: ParticipantDetailsContainerComponent, data: {title: 'Participant Details'},
      //     },
      //     {
      //       path: 'details', component: ParticipantDetailsComponent, data: {title: 'Participant Details'},
      //     },
      //     {
      //       path: 'modify',
      //       component: ParticipantModifyRequestComponent,
      //       data: {title: 'Participant Modification Request'}
      //     },
      //     {
      //       path: 'audit',
      //       children: [
      //         {
      //           path: '', component: ParticipantAuditComponent, data: {title: 'Participant Audit'},
      //         },
      //         {
      //           path: ':revisionId',
      //           component: ParticipantAuditDetailsComponent,
      //           data: {title: 'Participant Audit Details'}
      //         }
      //       ]
      //       , data: {title: 'Participant Audit'}
      //     }
      //   ]
      //   , data: {title: 'Participant Details'}
      // }
    ]
    , data: {title: 'Participants'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
