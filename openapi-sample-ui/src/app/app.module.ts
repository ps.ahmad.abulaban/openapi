import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ApiModule, BASE_PATH} from '../openapi';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {environment} from '../environments/environment';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ParticipantsGridComponent} from './participants/grid/participants-grid.component';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatExpansionModule} from '@angular/material/expansion';
import {NoRecordsFoundComponent} from './generic-components/no-records-found/no-records-found.component';
import {FilterPanelComponent} from './generic-components/filter/filter-panel.component';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {SideMenuComponent} from './side-menu/side-menu.component';
import {MatListModule} from '@angular/material/list';
import {ParticipantsDetailsComponent} from './participants/details/participants-details.component';
import {ParticipantsCreationComponent} from './participants/creation/participants-creation.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatPaginatorI18nService} from './mat-paginator-i18n-service';

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    NoRecordsFoundComponent,
    FilterPanelComponent,
    ParticipantsGridComponent,
    SideMenuComponent,
    ParticipantsDetailsComponent,
    ParticipantsCreationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ApiModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatSelectModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    ReactiveFormsModule,
    MatTooltipModule
  ],
  providers: [
    {
      provide: BASE_PATH,
      useValue: environment.api_uri
    },
    {
      provide: MatPaginatorIntl, deps: [TranslateService],
      useClass: MatPaginatorI18nService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
