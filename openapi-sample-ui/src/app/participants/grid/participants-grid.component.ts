import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {FilterPanelComponent} from '../../generic-components/filter/filter-panel.component';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
// @ts-ignore
import {Page, PageRequest, ParticipantResponse, ParticipantsService} from '../../../openapi';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-participants-grid',
  templateUrl: './participants-grid.component.html',
  styleUrls: ['./participants-grid.component.css']
})
export class ParticipantsGridComponent implements OnInit {

  dataSource: MatTableDataSource<ParticipantResponse> = new MatTableDataSource<ParticipantResponse>();
  pageInfo: Page;
  @ViewChild(FilterPanelComponent, {static: true}) panel: FilterPanelComponent;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  dataAvailable = false;
  // tslint:disable-next-line:new-parens
  pageRequest: PageRequest = new class implements PageRequest {
    page = 0;
    size = 10;
    sortBy = ['code'];
    sortDirection = 'desc';
  };
  participantTypes: string[] = ['Direct', 'Operator', 'Indirect'];
  private defaultValue = '---';
  codeFilter: string = null;
  bicFilter: string = null;
  nameFilter: string = null;
  participantTypeFilter: string = null;

  constructor(private snackBar: MatSnackBar,
              private participantsService: ParticipantsService) {
  }

  ngOnInit(): void {
    this.fetchData();
  }

  onPageEvent(event: PageEvent): void {
    this.pageRequest.size = event.pageSize;
    this.pageRequest.page = event.pageIndex;
    this.fetchData();
  }

  onSortChange(event: Sort): void {
    this.pageRequest.sortBy[0] = event.active;
    this.pageRequest.sortDirection = event.direction;
    this.fetchData();
  }

  private fetchData(): void {
    this.participantsService.getParticipants('code,bic,name,participantType', this.pageRequest,
      this.codeFilter, this.bicFilter, this.nameFilter, this.participantTypeFilter)
      .subscribe((p => {
        this.dataSource.data = p.content;
        this.pageInfo = p.page;
        this.dataAvailable = true;
      }), (e => {
        this.snackBar.open('Error while fetching participants list', '', {duration: 4000});
      }));
  }

  getDefaultValueIfNull(value: any): any {
    if (value) {
      return value;
    }
    return this.defaultValue;
  }

  resetDefaults(): void {
    this.pageRequest.page = 0;
    this.codeFilter = null;
    this.bicFilter = null;
    this.nameFilter = null;
    this.participantTypeFilter = null;
    this.fetchData();
  }

  applyFilter(): void {
    this.pageRequest.page = 0;
    this.fetchData();
  }

}
