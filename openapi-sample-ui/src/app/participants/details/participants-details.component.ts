import {Component, OnInit} from '@angular/core';
import {ParticipantResponse, ParticipantsService} from '../../../openapi';
import {ActivatedRoute, Params} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-participants-details',
  templateUrl: './participants-details.component.html',
  styleUrls: ['./participants-details.component.css']
})
export class ParticipantsDetailsComponent implements OnInit {
  participant: ParticipantResponse = null;
  private defaultValue = '---';
  dataAvailable: boolean;

  constructor(private snackBar: MatSnackBar,
              private activatedRoute: ActivatedRoute,
              private participantsService: ParticipantsService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.participantsService.getParticipant(params.code).subscribe(p => {
        // @ts-ignore
        console.log(p.links);
        this.participant = p;
        this.dataAvailable = true;
      }, (() => {
        this.snackBar.open('Error while fetching participants list', '', {duration: 4000});
      }));
    });
  }

  getDefaultValueIfNull(value: any): any {
    if (value) {
      return value;
    }
    return this.defaultValue;
  }

}
