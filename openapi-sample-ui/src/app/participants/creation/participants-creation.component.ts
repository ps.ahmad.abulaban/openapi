import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
// @ts-ignore
import {PageRequest, ParticipantCreationRequest, ParticipantResponse, ParticipantsService} from '../../../openapi';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Location} from '@angular/common';

@Component({
  selector: 'app-participants-creation',
  templateUrl: './participants-creation.component.html',
  styleUrls: ['./participants-creation.component.css']
})
export class ParticipantsCreationComponent implements OnInit {

  maxImageSize: number = 1024 * 1024;
  imageOversize: boolean;
  invalidImageFormat: boolean;
  logoBase64: string;
  formSubmitAttempt: boolean;
  participantTypes: string[] = ['Direct', 'Indirect'];
  requestForm: FormGroup;
  isSubmitted: boolean;
  participants: ParticipantResponse[] = [];
  isDirect: boolean;
  // tslint:disable-next-line:new-parens
  pageRequest: PageRequest = new class implements PageRequest {
    page = 0;
    size = 1000;
    sortBy = ['code'];
    sortDirection = 'desc';
  };

  constructor(private snackBar: MatSnackBar,
              private participantsService: ParticipantsService,
              public location: Location,
              private router: Router) {
  }

  ngOnInit(): void {
    this.requestForm = new FormGroup({
      code: new FormControl('', [Validators.required as any, Validators.pattern('^([A-Za-z0-9]+)$')]),
      bic: new FormControl('', []),
      name: new FormControl('', [Validators.required as any, Validators.pattern('^([0-9a-zA-Z]|([0-9a-zA-Z])([\\w\\s\\-\'])*[0-9a-zA-Z\'])$')]),
      shortName: new FormControl('', [Validators.required as any, Validators.pattern('^[A-Z]+$')]),
      logo: new FormControl('', []),
      participantType: new FormControl('Direct', [Validators.required as any]),
      settlementBank: new FormControl('', [])
    });
    this.onSelectType('Direct');
    this.listParticipants();
  }

  private listParticipants(): void {
    this.participantsService.getParticipants('code,name,participantType', this.pageRequest)
      .subscribe((response) => {
          response.content.forEach(p => {
            if (p.participantType === 'Direct') {
              this.participants.push(p);
            }
          });
        },
        (() => this.snackBar.open('Error while fetching participants list', '', {duration: 4000})));
  }

  isFieldValid(field: string): boolean {
    return (!this.requestForm.get(field).valid && this.requestForm.get(field).touched) ||
      (this.requestForm.get(field).untouched && this.formSubmitAttempt);
  }

  handleFileSelect(event): void {
    const files = event.target.files;
    const file: File = files[0];
    if (!file) {
      return;
    }
    this.imageOversize = file.size > this.maxImageSize;
    this.invalidImageFormat = !this.isValidImageFormat(file);

    if (!this.imageOversize && !this.invalidImageFormat) {
      const reader = new FileReader();
      reader.onload = this.handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }

  private isValidImageFormat(file: File): any {
    return file.type.startsWith('image/') && file.name.match('.png|.jpg|.jpeg|.gif');
  }

  handleReaderLoaded(event): void {
    const binaryString = event.target.result;
    this.logoBase64 = btoa(binaryString);
  }

  submitForm(): void {
    this.formSubmitAttempt = true;
    if (!this.isFormValid()) {
      return;
    }
    this.isSubmitted = true;
    const request = this.createRequest();
    this.participantsService.createParticipant(request)
      .subscribe((p) => this.redirectToRecord(p.code),
        (error) => {
          this.isSubmitted = false;
          this.snackBar.open('Error while creating participant ' + error, '', {duration: 4000});
        });
  }

  isFormValid(): any {
    return this.requestForm.valid && this.logoBase64;
  }

  redirectToRecord(code: string): void {
    this.router.navigate(['/participants/' + code], {replaceUrl: true});
  }

  private createRequest(): ParticipantCreationRequest {
    // tslint:disable-next-line:new-parens
    const request: ParticipantCreationRequest = new class implements ParticipantCreationRequest {
      bic: string | null;
      code: string;
      logo: string;
      name: string;
      participantType: string;
      settlementBank: string | null;
      shortName: string;
    };
    request.participantType = this.requestForm.controls.participantType.value;
    request.code = this.requestForm.controls.code.value;
    if (request.participantType === 'Direct') {
      request.bic = this.requestForm.controls.bic.value;
    }
    request.name = this.requestForm.controls.name.value;
    request.shortName = this.requestForm.controls.shortName.value;
    request.logo = this.logoBase64;
    if (request.participantType === 'Indirect') {
      request.settlementBank = this.requestForm.controls.settlementBank.value;
    }
    return request;
  }

  cancelClicked(): void {
    this.location.back();
  }

  onSelectType(event): void {
    const type = event.value;
    if (type === 'Indirect') {
      this.isDirect = false;
      this.requestForm.controls.bic.setValidators([]);
      this.requestForm.controls.bic.reset();
      this.requestForm.controls.bic.disable();
      this.requestForm.controls.settlementBank.setValidators([Validators.required as any]);
      this.requestForm.controls.settlementBank.reset();
      this.requestForm.controls.settlementBank.enable();
    } else {
      this.isDirect = true;
      this.requestForm.controls.bic.setValidators([Validators.required as any, Validators.pattern('^\\d{9}$')]);
      this.requestForm.controls.bic.reset();
      this.requestForm.controls.bic.enable();
      this.requestForm.controls.settlementBank.setValidators([]);
      this.requestForm.controls.settlementBank.reset();
      this.requestForm.controls.settlementBank.disable();
    }
  }

}
