import {ChangeDetectorRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {MediaMatcher} from '@angular/cdk/layout';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {filter, map, mergeMap} from 'rxjs/operators';
import {TranslateService} from '@ngx-translate/core';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(MatSidenav, {static: false}) public sidenav: MatSidenav;

  username = '';
  firstName: string;
  lastName: string;
  mobileQuery: MediaQueryList;
  title: string;
  productName: string;
  private readonly _mobileQueryListener: () => void;
  isPageLoaded: boolean;
  appVersion: string;
  dir: string;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              public router: Router,
              private translateService: TranslateService,
              @Inject(DOCUMENT) private document: Document,
              private activatedRoute: ActivatedRoute,
              private titleService: Title) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    const browserLang = translateService.getBrowserLang().match(/en|ar/) ? translateService.getBrowserLang() : 'en';
    this.dir = browserLang === 'ar' ? 'rtl' : 'ltr';
    translateService.use(browserLang);
    // this.productName = environment.product_name;
    // if (environment.environment_type && environment.environment_type.trim().length > 0) {
    //   this.productName = this.productName + ' - ' + environment.environment_type;
    // }
    // this.appVersion = environment.app_version;
  }

  async ngOnInit() {

    this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd),
      map(() => this.activatedRoute),
      map((route) => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }),
      filter((route) => route.outlet === 'primary'),
      mergeMap((route) => route.data))
      .subscribe((event) => {
          // this.titleService.setTitle(environment.product_name + ' - ' + event['title']);
          this.title = event.title;
        }
      );
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.isPageLoaded = true;
      }
    });
  }

  toggleSideMenu() {
    if (this.sidenav) {
      this.sidenav.toggle();
    }
  }

  changeLanguage(lang: string): void {
    this.dir = lang === 'ar' ? 'rtl' : 'ltr';
    const htmlTag = this.document.getElementsByTagName(
      'html'
    )[0] as HTMLHtmlElement;
    htmlTag.dir = this.dir;
    this.translateService.setDefaultLang(lang);
    this.translateService.use(lang);
  }
}
