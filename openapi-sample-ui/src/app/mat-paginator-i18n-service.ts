import {Injectable} from '@angular/core';
import {MatPaginatorIntl} from '@angular/material/paginator';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class MatPaginatorI18nService extends MatPaginatorIntl {

  public constructor(private translate: TranslateService) {
    super();

    this.translate.onLangChange.subscribe((e: Event) => {
      this.getAndInitTranslations();
    });

    this.getAndInitTranslations();
  }

  public getAndInitTranslations(): void {
    this.itemsPerPageLabel = this.translate.instant('paginator.ITEMS_PER_PAGE_LABEL');
    this.nextPageLabel = this.translate.instant('paginator.NEXT_PAGE_LABEL');
    this.previousPageLabel = this.translate.instant('paginator.PREVIOUS_PAGE_LABEL');
    this.firstPageLabel = this.translate.instant('paginator.FIRST_PAGE_LABEL');
    this.lastPageLabel = this.translate.instant('paginator.LAST_PAGE_LABEL');
    this.getRangeLabel = this.getRangeLabelTranslated.bind(this);
    this.changes.next();
  }

  getRangeLabelTranslated(page: number, pageSize: number, length: number): string {
    if (length === 0 || pageSize === 0) {
      return this.translate.instant('paginator.RANGE_PAGE_LABEL_1', {length});
    }
    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return this.translate.instant('paginator.RANGE_PAGE_LABEL_2', {startIndex: startIndex + 1, endIndex, length});
  }
}
