import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';
import {MatSidenav} from '@angular/material/sidenav';


@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent {

  @Input() router: Router;
  @Input() sidenav: MatSidenav;
  @Input() mobileQuery: MediaQueryList;

  // clearingLinks = ['messages-logs', 'messages', 'files-logs', 'transactions',
  //   'fees-logs', 'journal-vouchers', 'ncps', 'fee-distribution-jv'];

  sendToRoute(sRoute: string) {
    if (this.mobileQuery.matches) {
      this.sidenav.close();
      this.router.navigate([sRoute]);
    }
  }

  isAnyRouteActive(routes: string[]): boolean {
    let isActive = false;
    routes.forEach(route => {
      isActive = isActive || this.router.isActive(route, false);
    });
    return isActive;
  }
}
